package project1

class ClassListController {

    def index() {
        def classList = ClassList.findAll()
        [classList:classList]
        println 'class list = '+classList;
    }

    def createClass(){

    }

    def storeClass(){
        ClassList classList = new ClassList()

        classList.class_no = params.class_no

        if(classList.save(flush: true, failOnError: true)){
            redirect(action: 'index')
        }else {
            render "Failed" //  yo txt dekhaune intel j 17 ko chamtkaar ho haiok
        }
    }
    def editClass(){
        def classList = ClassList.findById(params.id) // haina,, edit garna lai tyo euta. ok
        [classList: classList]
    }


    def updateClass(){
        ClassList classList = ClassList.get(params.id)
        classList.class_no = params.class_no

        if(classList.save(flush: true, failOnError: true)){
            redirect(action: 'index')
        }else {
            render"Failed"//
        }

    }

    def deleteClass(){
        def classList = ClassList.findById(params.id)
        classList.delete(flush: true)
        redirect(action: 'index')
    }
}
