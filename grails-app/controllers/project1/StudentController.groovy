package project1

import groovy.sql.Sql

class StudentController {
    // yaha vitra chai already banakoor new service callgarne invoke garne mysql ko lagi datasource vanne server

    def dataSource


    def index() {
        def studentList = Student.findAll()
        [studentList: studentList]
    }

    def createStudent(){
        def sql = Sql.newInstance(dataSource)
        def query = sql.rows("SELECT * FROM CLASS_LIST ;") // yaha chai real table ko name lekhne
        sql.close()
//        println "query"+query;
//        def classList = ClassList.findAll()
        // sab class ko name pathaune teivayer aba view ma pathau
        [classList:query]
    }
    def storeStudent(){
        println "parmas"+ params
        Student student = new Student();

        student.fullName = params.name
        student.rollNo = params.roll
        // aba yaha chhora janmada baau ko name rakhna parchha.. baau vanya purai object ho yaha. so baau ko object line pahila
        ClassList classList = ClassList.findById(params.classId) // aba bau chinyo chorale id pathauchha bauko

        student.classLIst = classList; // bau lai chorama assign gareko//
        if(student.save(flush: true, failOnError: true)){
            flash.message = " Saved Successfully"
            redirect(action: 'index')
        }else {
            flash.message = " Saved Failed"

        }
    }
    def editStudent(){
        def student = Student.findById(params.id) //tei garya xa ae ae maile gades budako jasto ajax wale sochixu :( haha,, nm
        [student: student]
    }

    def updateStudent(){
        println 'params ' + params // doen't matter but pahilai print garne ho agadi nai aauchha ni youmm
        Student student = Student.get(params.id)
        println 'old student' + student

        student.fullName = params.name
        student.rollNo = params.roll
        //baau banaune
        def class_list = ClassList.get(params.id) // yaha bauko id bata object banaune
        student.classLIst = params.class // classList is istance of ClassList CLass

//        render(student.fullName) /// hyaa its flush bro... toilet ko flush not flash light.test bro where are yiu now?
        if(student.save(flush: true, failOnError: true)){
            redirect(action: 'index')
        }else {
            render "Failed"
        }

    }
    // aba check garam sir vayen vane ma herdinchhu

    def deleteStudent(){
        def student = Student.findById(params.id)
        student.delete(flush: true)
        redirect(action: 'index')
    }
}
