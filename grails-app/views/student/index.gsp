<%--
  Created by IntelliJ IDEA.
  User: SocialAvesDev1
  Date: 8/22/2017
  Time: 11:53 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<g:if test="${flash.message}">
    <div class="alert alert-success fade in">
        <button class="close" data-dismiss="alert">x</button>
        <i class="fa-fw fa fa-check"></i>${flash.message}
    </div>
</g:if>
%{--<g:if test="${flash.message}">-
   %{--<p>${flash.message} </p>--}%
    %{--</g:if>--}%
%{--<g:else>--}%
    %{----}%
%{--</g:else>--}%


<div class="container">
    <h2>Student List</h2>
    <g:link controller="student" action="createStudent"> <button class="btn btn-primary btn-sm pull-right"> Add </button> </g:link>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Full Name</th>
            <th>Class</th>
            <th>Roll No</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${studentList}" var="sl">
            <tr>
                <td>${sl.fullName}</td>
                <td>${sl.classLIst.class_no}</td>
                <td>${sl.rollNo}</td>
                <td><g:link controller="student" action="deleteStudent" id="${sl.id}"><button class="btn btn-danger btn-sm"> Delete </button></g:link></td>
                <td><g:link controller="student" action="editStudent" id="${sl.id}"><button class="btn btn-info btn-sm"> Edit </button></g:link></td>
            </tr>
        </g:each>


        </tbody>
    </table>
</div>

</body>
</html>
