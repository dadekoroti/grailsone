<%--
  Created by IntelliJ IDEA.
  User: SocialAvesDev1
  Date: 8/22/2017
  Time: 11:57 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <g:link controller="student" action="index"> <button class="btn btn-primary btn-block"> List </button> </g:link>
    <g:form controller="student" action="storeStudent">
        <div class="form-group">
            <label for="name">Full Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
        </div>

        <div class="form-group">
            <label for="roll">Roll No:</label>
            <input type="text" class="form-control" id="roll" placeholder="Enter Roll No" name="roll">
        </div>
        <div class="form-group">
            <g:formatDate format="yyyy-MM-dd" date="${date}"/>
        </div>
        <div class="form-group">
            <label for="roll">Select Class:</label>
            <select name="classId" class="form-control">
                <g:each  in="${classList}" var="cl">

                    <option value="${cl.id}">${cl.class_no}</option>

                </g:each>

            </select>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </g:form>
</div>

</body>
</html>