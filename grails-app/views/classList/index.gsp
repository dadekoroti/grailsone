<%--
  Created by IntelliJ IDEA.
  User: SocialAvesDev1
  Date: 8/22/2017
  Time: 5:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Class List</h2>
    <g:link controller="classList" action="createClass"> <button class="btn btn-primary btn-sm pull-right"> Add </button> </g:link>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Class</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${classList}" var="cl">
            <tr>
                <td>${cl.id}</td>
                <td>${cl.class_no}</td>
                <td><g:link controller="classList" action="deleteClass" id="${cl.id}"><button class="btn btn-danger btn-sm"> Delete </button></g:link></td>
                <td><g:link controller="classList" action="editClass" id="${cl.id}"><button class="btn btn-info btn-sm"> Edit </button></g:link></td>
            </tr>
        </g:each>


        </tbody>
    </table>
</div>

</body>
</html>