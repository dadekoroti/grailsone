<%--
  Created by IntelliJ IDEA.
  User: SocialAvesDev1
  Date: 8/22/2017
  Time: 5:41 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <g:form controller="student" action="updateStudent">
        <div class="form-group">
            <label for="name">Class:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Name" value="${classList.class_no}" name="name">
        </div>

        <input type="hidden" name="id" value="${classList.id}">
        <button type="submit" class="btn btn-default">Submit</button>
    </g:form>
</div>

</body>
</html>