<%--
  Created by IntelliJ IDEA.
  User: SocialAvesDev1
  Date: 8/22/2017
  Time: 5:28 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <g:link controller="classList" action="index"> <button class="btn btn-primary btn-block"> List </button> </g:link>
    <g:form controller="classList" action="storeClass">
        <div class="form-group">
            <label for="class">Class:</label>
            <input type="text" class="form-control" id="class" placeholder="Enter Class" name="class_no">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </g:form>
</div>

</body>
</html>